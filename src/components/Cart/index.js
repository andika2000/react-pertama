import React from 'react';
import {View, Text} from 'react-native';

const Cart = (props) => {
    return(
        <View>
            <Text style={{fontSize:24, fontWeight:'bold'}}>Jumlah Belanja</Text>
            <Text style={{fontSize:20, fontWeight:'bold'}}>{props.value}</Text>
        </View>
    );
}

export default Cart;