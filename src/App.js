import React from 'react';
import {View} from 'react-native';
import CallApi from './pages/CallApi';

const App = () => {
  return (
    <View>
      <CallApi/>
    </View>
    
  );
};

export default App;