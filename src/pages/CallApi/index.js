import React, { useEffect, useState } from 'react';
import {Text, View, StyleSheet, Button, Image} from 'react-native';

const CallApi = () => {
    const [dataUser, setDataUser] = useState({
        avatar: '',
        email: '',
        first_name: '',
        id: '',
        last_name: ''
    });

    useEffect(() => {

        // fetch data dengan method GET
        // fetch('https://reqres.in/api/users/2')
        // .then(response => response.json())
        // .then(json => console.log(json))
        
        // fetch data dengan method POST
        const dataForApi = {
            name: "morpheus",
            job: "leader"
        }
        fetch('https://reqres.in/api/users', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify(dataForApi)
        })
        .then(response => response.json())
        .then(json => console.log(json))
    }, []);

    // fungsi get data API 
    const getData = () => {
        // fetch data dengan method GET
        fetch('https://reqres.in/api/users/3')
        .then(response => response.json())
        .then(json => setDataUser(json.data))
    }

    return(
        <View>
            <View style={style.view}>
                <Image source={{uri: dataUser.avatar}} style={style.img}/>
                <Text>{`Nama : ${dataUser.first_name} ${dataUser.last_name}`}</Text>
                <Text>{`Email : ${dataUser.email}`}</Text>
                <Text>{`Id : ${dataUser.id}`}</Text>
            </View>
            <View style={style.btn}>
                <Button title="get data" onPress={getData}/>
            </View>
        </View>
    );
}

const style = StyleSheet.create({
    view: {
        textAlign: 'center',
        marginTop: 20,
        marginLeft: 150,
        
    },
    btn: {
        width: 100,
        marginLeft: 125,
        marginTop: 15
    },
    img: {
        width: 50,
        height: 50
    }
    
});

export default CallApi;