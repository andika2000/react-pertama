import React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import comp from '../../assets/image/comp.jpg';

const Card = () => {

    return(
        <View style={CardStyle.view}>
            <Image source={comp} 
            style={{
                width: 150,
                height: 150,
                borderRadius: 8
            }}/>
            <Text style={CardStyle.text}>MAC BOOK PRO</Text>
            <Text style={CardStyle.text2}>Rp. 25.000.000.00</Text>
            <Text style={CardStyle.text3}>Bandung</Text>
        </View>
        
        );
    
};

const CardStyle = StyleSheet.create({
    view:{
        padding: 12,
        width: 175,
        backgroundColor: '#f2f2f2',
        marginLeft: 100,
        marginTop: 15,
        borderRadius: 8
    },

    text:{
        fontWeight: "bold",
        marginTop:10
    },

    text2:{
        color: 'orange',
        marginTop: 10
    },

    text3:{
        marginTop: 10
    }
});

export default Card;