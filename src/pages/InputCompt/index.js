import React, {Component} from 'react';
import {View, TextInput} from 'react-native';

class InputComp extends Component {
    render() {
      return(
        <View>
          <TextInput
          style={{
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
            marginTop: 50,
            width: 300,
            marginLeft: 25,
            borderRadius: 8
          }}
          defaultValue="type here...."
        />
        </View>
      );
    };
  };

  export default InputComp;