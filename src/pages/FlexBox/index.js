import React, {Component, useEffect, useState} from 'react';
import { View, Text } from 'react-native';

// class FlexBox extends Component{
//     constructor(props){
//         super(props);
//         console.log('===> constructor');
//         this.state = {
//             text: 200,
//         }
//     }

//     componentDidMount(){
//         console.log('===> component did mont');
//         setTimeout(() => {
//             this.setState({
//                 text: 500,
//             });
//         }, 2000);
//     }

//     componentDidUpdate(){
//         console.log('===> component did update');
//     }

//     render(){
//         console.log('===> render');
//         return(
//             <View>
//                 <View style={{flexDirection:'row', backgroundColor:'#f2f2f2', justifyContent:'space-between'}}>
//                     <View style={{backgroundColor:'red', width:50, height:50}}/>
//                     <View style={{backgroundColor:'green', width:50, height:50}}/>
//                     <View style={{backgroundColor:'blue', width:50, height:50}}/>
//                     <View style={{backgroundColor:'black', width:50, height:50}}/>
//                     <Text>{this.state.text}</Text>
//                 </View>
//                 <Nav/>
//             </View>
//         );
//     }
// }

const Nav = () => {
    const [data, setData] = useState(200);

    useEffect(() => {
        console.log('did mount');
    }, []);

    useEffect(() => {
        console.log('did update');

        setTimeout(() => {
            setData(400);
        }, 2000);
        
    }, [data]);

    return(
        <View style={{flexDirection:'row', justifyContent:'space-around', marginTop:30}}>
            <Text>Home</Text>
            <Text>About</Text>
            <Text>Contact</Text>
            <Text>Services</Text>
            <Text>{data}</Text>
        </View>
    );
};

const FlexBox = () => {
    return(
        <View>
                <View style={{flexDirection:'row', backgroundColor:'#f2f2f2', justifyContent:'space-between'}}>
                    <View style={{backgroundColor:'red', width:50, height:50}}/>
                    <View style={{backgroundColor:'green', width:50, height:50}}/>
                    <View style={{backgroundColor:'blue', width:50, height:50}}/>
                    <View style={{backgroundColor:'black', width:50, height:50}}/>
                </View>
                <Nav/>
            </View>
    );
}

export default FlexBox;