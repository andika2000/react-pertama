import React from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
import comp from '../../assets/image/comp.jpg';

const Card = (props) => {

    return(
        <View style={CardStyle.view}>
            <Image source={comp} 
            style={{
                width: 150,
                height: 150,
                borderRadius: 8
            }}/>
            <Text style={CardStyle.text}>{props.judul}</Text>
            <Text style={CardStyle.text2}>{props.harga}</Text>
            <Text style={CardStyle.text3}>{props.lokasi}</Text>
        </View>
        
        );
    
};

const CardStyle = StyleSheet.create({
    view:{
        padding: 12,
        width: 175,
        backgroundColor: '#f2f2f2',
        marginLeft: 100,
        marginTop: 15,
        borderRadius: 8
    },

    text:{
        fontWeight: "bold",
        marginTop:10
    },

    text2:{
        color: 'orange',
        marginTop: 10
    },

    text3:{
        marginTop: 10
    }
});

const PropsDinamis = () => {
    return(
        <View>
            <ScrollView>
                <Card judul='PROPS 1' harga='RP. 30.000.000.00' lokasi='madiun'/>
                <Card judul='PROPS 2' harga='Rp. 11.000.000.00' lokasi='magelang'/>
                <Card judul='PROPS 3' harga='Rp. 13.000.000.00' lokasi='makasar'/>
            </ScrollView>
        </View>
    );
}

export default PropsDinamis;