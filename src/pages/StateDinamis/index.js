import React, { Component, useState } from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

const Counter = () => {
    const [number, setNumber] = useState(0);

    return(
        <View style={style.counter}>
            <Text style={{fontSize:22, fontWeight:'bold'}}>Functional Component</Text>
            <Text style={style.text}>{number}</Text>
            <Button title='tambah' onPress={() => setNumber(number + 1)}/>
            <View style={{marginTop:10}}>
                <Button title='reset' onPress={() => setNumber(0)} />
            </View>
        </View>
    );
}

class CounterClass extends Component {
    state = {
        number: 0
    }

    render() {
        return(
            <View style={style.counter}>
                <Text style={{fontSize:22, fontWeight:'bold'}}>Class Component</Text>
                <Text style={style.text}>{this.state.number}</Text>
                <Button title='tambah' onPress={() => this.setState({number: this.state.number + 1})}/>
                <View style={{marginTop:10}}>
                    <Button title='reset' onPress={() => this.setState({number: this.state.number = 0})} />
                </View>
            </View>
        );
    }
}

const StateDinamis = () => {
    return(
        <View style={style.view}>
            <Counter/>
            <CounterClass/>
        </View>
    );
}

const style = StyleSheet.create({
    text: {
        fontSize: 24,
        textAlign: 'center',
        marginBottom: 20
    },

    view: {
        marginTop: 30
    },

    counter: {
        padding: 30,
    }
});

export default StateDinamis;