import React, { useState } from 'react';
import {View, Text} from 'react-native';
import Product from '../../components/Product';
import Cart from '../../components/Cart';

const Comunication = () => {
    const [touch, setTouch] = useState(0);
    return(
        <View>
            <Cart value={touch}/>
            <Product onButtonPress={() => setTouch(touch + 1)}/>
        </View>
    );
}

export default Comunication;